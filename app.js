const express = require('express');
const app = express();

const PORT = process.env.PORT || 8877;
app.get('/equips',(req,res)=>{
    res.json({
        cpu: 'AMD',
        gpu: 'Galax',
        mouse: 'Logitech',
        headset: 'Turtle',
        keayboard: 'Alfawise',
    })
})

app.get('/about',(req,res)=>{
    res.json({
        name:'Webhe',
        email:'webhe.costa@hallen.com.br',
        urls:[
            {
                type:'github',
                url: 'https://gitlab.com/webhecosta'
            },
            {
                type:'hallen',
                url: 'http://177.53.171.230:8882'
            },
            {
                type:'kinghost',
                url: 'www.kinghost.net'
            }

        ]
    })
})
app.get('/',(req,res)=>{
    res.json({
        msg: 'OK'
    })
})


app.listen(PORT,() => {
    console.log('Escutando na porta : ' + PORT);
})